package pl.designer;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class BinaryDataFactory {

    private final ByteArrayConverter byteArrayConverter;

    byte[] toData(List<Triangle2D> triangles) {
        List<Float> coordinates = triangles.stream()
                .flatMap(this::toFloatStream)
                .collect(toList());
        return byteArrayConverter.toByteArray(coordinates);
    }

    private Stream<Float> toFloatStream(Triangle2D triangle) {
        return Stream.of(triangle.getP1(), triangle.getP2(), triangle.getP3())
                .flatMap(p -> Stream.of(p.getX(), p.getY(), p.getZ()));
    }
}
