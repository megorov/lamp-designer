package pl.designer;

import lombok.Value;

@Value
public final class Triangle2D {

    Point2D p1;
    Point2D p2;
    Point2D p3;
}
