package pl.designer;

import de.javagl.jgltf.impl.v2.Accessor;
import de.javagl.jgltf.impl.v2.Asset;
import de.javagl.jgltf.impl.v2.Buffer;
import de.javagl.jgltf.impl.v2.BufferView;
import de.javagl.jgltf.impl.v2.GlTF;
import de.javagl.jgltf.impl.v2.Image;
import de.javagl.jgltf.impl.v2.Material;
import de.javagl.jgltf.impl.v2.MaterialPbrMetallicRoughness;
import de.javagl.jgltf.impl.v2.Mesh;
import de.javagl.jgltf.impl.v2.MeshPrimitive;
import de.javagl.jgltf.impl.v2.Node;
import de.javagl.jgltf.impl.v2.Sampler;
import de.javagl.jgltf.impl.v2.Scene;
import de.javagl.jgltf.impl.v2.Texture;
import de.javagl.jgltf.impl.v2.TextureInfo;
import de.javagl.jgltf.model.io.GltfWriter;
import lombok.Builder;
import lombok.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import static de.javagl.jgltf.model.GltfConstants.GL_ARRAY_BUFFER;
import static de.javagl.jgltf.model.GltfConstants.GL_FLOAT;
import static de.javagl.jgltf.model.GltfConstants.GL_LINEAR;
import static de.javagl.jgltf.model.GltfConstants.GL_LINEAR_MIPMAP_LINEAR;
import static de.javagl.jgltf.model.GltfConstants.GL_REPEAT;

@Builder
class GltfModel {

    @NonNull
    private final byte[] meshData;
    @NonNull
    private final byte[] textureData;
    @NonNull
    private final int trianglesCount;
    @NonNull
    private final BoundBox boundBox;

    String toJson() throws IOException {
        try (var outputStream = new ByteArrayOutputStream()) {
            GlTF gltf = gltf();
            GltfWriter gltfWriter = new GltfWriter();
            gltfWriter.write(gltf, outputStream);
            return outputStream.toString();
        }
    }

    private GlTF gltf() {
        var gltf = new GlTF();
        gltf.setAsset(asset());
        gltf.addMeshes(mesh());
        gltf.addBuffers(buffer(textureData));
        gltf.addBuffers(buffer(meshData));
        gltf.addBufferViews(bufferView(textureData, 0));
        gltf.addBufferViews(bufferView(meshData, 1));
        gltf.addAccessors(textureAccessor());
        gltf.addAccessors(meshAccessor());
        gltf.addScenes(scene());
        gltf.addNodes(node());
        gltf.addTextures(texture());
        gltf.addImages(image());
        gltf.addSamplers(sampler());
        gltf.addMaterials(material());
        return gltf;
    }

    private Material material() {
        var pbrMetallicRoughness = new MaterialPbrMetallicRoughness();
        pbrMetallicRoughness.setMetallicFactor(0f);
        pbrMetallicRoughness.setRoughnessFactor(1f);

        var baseColorTexture = new TextureInfo();
        baseColorTexture.setIndex(0);
        pbrMetallicRoughness.setBaseColorTexture(baseColorTexture);

        var material = new Material();
        material.setPbrMetallicRoughness(pbrMetallicRoughness);
        material.setDoubleSided(true);
        return material;
    }

    private Sampler sampler() {
        Sampler sampler = new Sampler();
        sampler.setMagFilter(GL_LINEAR);
        sampler.setMinFilter(GL_LINEAR_MIPMAP_LINEAR);
        sampler.setWrapS(GL_REPEAT);
        sampler.setWrapT(GL_REPEAT);
        return sampler;
    }

    private Image image() {
        Image image = new Image();
        image.setUri("../images/wood-600.jpg");
        return image;
    }

    private Texture texture() {
        Texture texture = new Texture();
        texture.setSource(0);
        texture.setSampler(0);
        return texture;
    }

    private Buffer buffer(byte[] data) {
        Buffer buffer = new Buffer();
        buffer.setByteLength(data.length);
        String dataUriString = base64Encoded(data);
        buffer.setUri(dataUriString);
        return buffer;
    }

    private String base64Encoded(byte[] data) {
        String base64EncodedData = Base64.getEncoder().encodeToString(data);
        return "data:application/gltf-buffer;base64," + base64EncodedData;
    }

    private Node node() {
        Node node = new Node();
        node.setMesh(0);
        return node;
    }

    private Scene scene() {
        Scene scene = new Scene();
        scene.addNodes(0);
        return scene;
    }

    private Asset asset() {
        Asset asset = new Asset();
        asset.setVersion("2.0");
        return asset;
    }

    private BufferView bufferView(byte[] data, int bufferIndex) {
        var bufferView = new BufferView();
        bufferView.setBuffer(bufferIndex);
        bufferView.setByteOffset(0);
        bufferView.setByteLength(data.length);
        bufferView.setTarget(GL_ARRAY_BUFFER);
        return bufferView;
    }
    private Accessor textureAccessor() {
        var accessor = new Accessor();
        accessor.setBufferView(0);
        accessor.setByteOffset(0);
        accessor.setComponentType(GL_FLOAT);
        accessor.setCount(trianglesCount * 3);
        accessor.setType("VEC2");
        return accessor;
    }

    private Accessor meshAccessor() {
        var accessor = new Accessor();
        accessor.setBufferView(1);
        accessor.setByteOffset(0);
        accessor.setComponentType(GL_FLOAT);
        accessor.setCount(trianglesCount * 3);
        accessor.setType("VEC3");
        accessor.setMax(meshBoundMax());
        accessor.setMin(meshBoundMin());
        return accessor;
    }

    private Float[] meshBoundMax() {
        Point2D max = boundBox.getMax();
        return coordinatesArray(max);
    }

    private Float[] meshBoundMin() {
        Point2D min = boundBox.getMin();
        return coordinatesArray(min);
    }

    private Float[] coordinatesArray(Point2D min) {
        return new Float[]{min.getX(), min.getY(), min.getZ()};
    }

    private Mesh mesh() {
        var mesh = new Mesh();
        var primitive = primitive();
        mesh.setPrimitives(List.of(primitive));
        return mesh;
    }

    private MeshPrimitive primitive() {
        var primitive = new MeshPrimitive();
        primitive.setMode(4); //TRIANGLES
        primitive.setAttributes(Map.of(
                "POSITION", 1,
                "TEXCOORD_0", 0));
        primitive.setMaterial(0);
        return primitive;
    }
}
