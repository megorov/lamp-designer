package pl.designer;

import lombok.Value;

@Value
public class BoundBox {
    private Point2D max;
    private Point2D min;
}
