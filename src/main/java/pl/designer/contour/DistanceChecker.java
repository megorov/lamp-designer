package pl.designer.contour;

interface DistanceChecker<T> {

    boolean ifPointsTooFar(T current, T next);
}
