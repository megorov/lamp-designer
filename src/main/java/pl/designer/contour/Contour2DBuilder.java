package pl.designer.contour;

import lombok.Builder;
import lombok.NonNull;
import pl.designer.Point2D;

import java.util.List;

import static java.lang.Math.toRadians;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

@Builder
public class Contour2DBuilder {

    @NonNull
    private final Contour2DConfiguration configuration;

    public Contour2D build() {
        var initialPoints = getInitialPoints();
        var enrichedPoints = enrich(initialPoints);
        var resultPoints = toPoint2D(enrichedPoints);
        return new Contour2D(resultPoints);
    }

    private List<Point2D> toPoint2D(List<RadianPoint2D> radianPoints) {
        return radianPoints.stream()
                .map(RadianPoint2D::getPoint2D)
                .collect(toList());
    }

    private List<RadianPoint2D> enrich(List<RadianPoint2D> contourPoints) {
        return new ContourEnricher<>(this::ifTooFarFromEachOther, this::createMiddlePoint)
                .getContour(contourPoints);
    }

    private RadianPoint2D createMiddlePoint(RadianPoint2D point1, RadianPoint2D point2) {
        double radianDiff = (point2.getRadians() - point1.getRadians()) / 2;
        double newPointRadians = point1.getRadians() + radianDiff;
        return fromRadiansToRadianPoint(newPointRadians);
    }

    private boolean ifTooFarFromEachOther(RadianPoint2D point1, RadianPoint2D point2) {
        Point2D p1 = point1.getPoint2D();
        Point2D p2 = point2.getPoint2D();
        double distance = p1.getDistance(p2);
        return distance > configuration.getMaxDistanceBetweenNodes();
    }

    private List<RadianPoint2D> getInitialPoints() {
        int startAngle = configuration.getAngleRange().getStartAngle();
        int finishAngle = configuration.getAngleRange().getFinishAngle();
        return range(startAngle, finishAngle)
                .mapToObj(this::getPoint2D)
                .collect(toList());
    }

    private RadianPoint2D getPoint2D(int angle) {
        double radians = toRadians(angle);
        return fromRadiansToRadianPoint(radians);
    }

    private RadianPoint2D fromRadiansToRadianPoint(double radians) {
        var radianFunction2D = configuration.getRadianFunction2D();
        var point2D = radianFunction2D.getPoint(radians);
        return new RadianPoint2D(radians, point2D);
    }
}
