package pl.designer.contour;

import lombok.Value;
import pl.designer.AngleRange;
import pl.designer.RadianFunction2D;

@Value
public class Contour2DConfiguration {

    RadianFunction2D radianFunction2D;
    double maxDistanceBetweenNodes;
    AngleRange angleRange;

}
