package pl.designer.contour;

public interface NewPointCreator<T> {

    T create(T point1, T point2);

}
