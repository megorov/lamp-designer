package pl.designer.contour;


import lombok.Value;
import pl.designer.Point2D;

@Value
class RadianPoint2D {

    double radians;
    Point2D point2D;
}
