package pl.designer.contour;

import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
public class ContourEnricher<T> {

    private final DistanceChecker<T> distanceChecker;
    private final NewPointCreator<T> newPointCreator;

    @RequiredArgsConstructor
    private static class Node<T> {

        final T element;
        Node<T> next = null;

        void insertBefore(Node<T> node) {
            node.next = this.next;
            this.next = node;
        }
    }

    public List<T> getContour(List<T> seedPoints) {
        Objects.requireNonNull(seedPoints);
        if (seedPoints.size() < 2) {
            throw new IllegalArgumentException("Seed points collection should have at lease 2 elements");
        }

        var root = toLinkedNodes(seedPoints);
        var current = root;
        while (current.next != null) {
            T currentElement = current.element;
            T nextElement = current.next.element;
            if (ifMiddlePointNeeded(currentElement, nextElement)) {
                T newOne = newPointCreator.create(currentElement, nextElement);
                current.insertBefore(new Node<>(newOne));
            } else {
                current = current.next;
            }
        }
        return toList(root);
    }

    private List<T> toList(Node<T> root) {
        List<T> result = new ArrayList<>();
        result.add(root.element);
        var current = root;
        while (current.next != null) {
            current = current.next;
            result.add(current.element);
        }
        return result;
    }

    private Node<T> toLinkedNodes(List<T> seedPoints) {
        var iterator = seedPoints.iterator();
        var begin = new Node<>(iterator.next());
        var current = begin;
        while (iterator.hasNext()) {
            var newNode = new Node<>(iterator.next());
            current.insertBefore(newNode);
            current = newNode;
        }
        return begin;
    }

    private boolean ifMiddlePointNeeded(T current, T next) {
        return distanceChecker.ifPointsTooFar(current, next);
    }
}
