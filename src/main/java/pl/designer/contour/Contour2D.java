package pl.designer.contour;

import pl.designer.Point2D;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static java.util.List.copyOf;

public class Contour2D {

    private final List<Point2D> points;
    private double length;

    public Contour2D(List<Point2D> contourPoints) {
        Objects.requireNonNull(contourPoints);
        if (contourPoints.size() < 2) {
            throw new IllegalArgumentException("Contour point  collection should have at lease 2 elements");
        }
        this.points = copyOf(contourPoints);
    }

    public double getLength() {
        if (length == 0) {
            length = lengthContourLength();
        }
        return length;
    }

    private double lengthContourLength() {
        var iterator = points.iterator();
        var first = iterator.next();

        var current = first;
        var next = iterator.next();
        double result = current.getDistance(next);

        while (iterator.hasNext()) {
            current = next;
            next = iterator.next();
            result += current.getDistance(next);
        }

        result += next.getDistance(first);
        return result;
    }

    public Stream<Point2D> getPointStream() {
        return points.stream();
    }
}
