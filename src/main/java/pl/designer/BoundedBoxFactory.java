package pl.designer;

import org.springframework.stereotype.Service;
import pl.designer.contour.Contour2D;

import static java.lang.Math.max;
import static java.lang.Math.min;

@Service
class BoundedBoxFactory {

    private static class Box {
        float maxX = 0;
        float maxY = 0;
        float minX = 0;
        float minY = 0;
    }

    BoundBox create(Contour2D contour) {
        Box box = box(contour);
        Point2D max = new Point2D(box.maxX, box.maxY);
        Point2D min = new Point2D(box.minX, box.minY);
        return new BoundBox(max, min);
    }

    private Box box(Contour2D contour) {
        return contour.getPointStream().reduce(new Box(), this::accumulator, (box1, box2) -> box1);
    }

    private Box accumulator(Box box, Point2D point) {
        box.maxX = max(point.getX(), box.maxX);
        box.maxY = max(point.getY(), box.maxY);

        box.minX = min(point.getX(), box.minX);
        box.minY = min(point.getY(), box.minY);
        return box;
    }
}
