package pl.designer.triangulation;

import org.poly2tri.Poly2Tri;
import org.poly2tri.geometry.polygon.Polygon;
import org.poly2tri.geometry.polygon.PolygonPoint;
import org.poly2tri.triangulation.TriangulationPoint;
import org.poly2tri.triangulation.delaunay.DelaunayTriangle;
import org.springframework.stereotype.Service;
import pl.designer.Point2D;
import pl.designer.Triangle2D;
import pl.designer.contour.Contour2D;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class TriangulationBuilder {

    public List<Triangle2D> triangulate(Contour2D contour) {

        var points = contourToPolygonPoints(contour);
        var polygon = new Polygon(points);
        Poly2Tri.triangulate(polygon);
        var triangles = polygon.getTriangles();

        return toTriangles(triangles);
    }

    private List<Triangle2D> toTriangles(List<DelaunayTriangle> triangles) {
        return triangles.stream()
                .map(delaunayTriangle -> delaunayTriangle.points)
                .map(this::getTriangle2D)
                .collect(toList());
    }

    private Triangle2D getTriangle2D(TriangulationPoint[] points) {
        var p1 = triangulationPointToPoint2D(points[0]);
        var p2 = triangulationPointToPoint2D(points[1]);
        var p3 = triangulationPointToPoint2D(points[2]);
        return new Triangle2D(p1, p2, p3);
    }

    private Point2D triangulationPointToPoint2D(TriangulationPoint point) {
        return new Point2D(point.getXf(), point.getYf());
    }

    private List<PolygonPoint> contourToPolygonPoints(Contour2D contour) {
        return contour.getPointStream()
                .map(point2D -> new PolygonPoint(point2D.getX(), point2D.getY()))
                .collect(toList());
    }
}
