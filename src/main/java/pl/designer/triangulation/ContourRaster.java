package pl.designer.triangulation;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static java.util.List.copyOf;

class ContourRaster {

    private final List<PointRaster> points;

    ContourRaster(List<PointRaster> contourPoints) {
        Objects.requireNonNull(contourPoints);
        if (contourPoints.size() < 2) {
            throw new IllegalArgumentException("Contour point  collection should have at lease 2 elements");
        }
        this.points = copyOf(contourPoints);
    }

    Stream<PointRaster> getPointStream() {
        return points.stream();
    }
}
