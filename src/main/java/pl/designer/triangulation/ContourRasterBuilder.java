package pl.designer.triangulation;

import lombok.RequiredArgsConstructor;
import pl.designer.contour.Contour2D;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.round;
import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
class ContourRasterBuilder {

    private final PointRasterBuilder pointRasterBuilder;

    ContourRaster toRasterized(Contour2D contour2D) {
        var rasterized = translateToRasterPoints(contour2D);
        var enriched = enrichAbsentPoints(rasterized);
        var distinct = distinctPoints(enriched);
        return new ContourRaster(distinct);
    }

    private List<PointRaster> distinctPoints(List<PointRaster> rasterized) {
        return rasterized.stream()
                .distinct()
                .collect(toList());
    }

    private List<PointRaster> enrichAbsentPoints(List<PointRaster> points) {
        List<PointRaster> result = new ArrayList<>();
        PointRaster[] pointArray = points.toArray(PointRaster[]::new);
        for (int i = 0; i < pointArray.length - 1; i++) {
            PointRaster current = pointArray[i];
            PointRaster next = pointArray[i + 1];
            result.add(current);
            if (current.isFarFromTheCurrentMoreThen1Pixel(next)) {
                List<PointRaster> enrichedBlock = enrichAbsentBetweenPoints(current, next);
                result.addAll(enrichedBlock);
            }
        }
        var last = pointArray[pointArray.length - 1];
        result.add(last);
        return result;
    }

    private List<PointRaster> enrichAbsentBetweenPoints(PointRaster current, PointRaster next) {
        int dx = next.getX() - current.getX();
        int dy = next.getY() - current.getY();
        int maxAbsoluteD = max(abs(dx), abs(dy));
        double offsetX = (double) dx / maxAbsoluteD;
        double offsetY = (double) dy / maxAbsoluteD;

        return IntStream.rangeClosed(1, maxAbsoluteD)
                .mapToObj(step -> getPointRaster(current, offsetX, offsetY, step))
                .collect(toList());
    }

    private PointRaster getPointRaster(PointRaster point, double offsetX, double offsetY, int value) {
        int x = (int) (point.getX() + round(value * offsetX));
        int y = (int) (point.getY() + round(value * offsetY));
        return new PointRaster(x, y);
    }

    private List<PointRaster> translateToRasterPoints(Contour2D contour2D) {
        return contour2D.getPointStream()
                .map(pointRasterBuilder::toRasterized)
                .collect(toList());
    }
}
