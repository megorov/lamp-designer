package pl.designer.triangulation;

import lombok.Value;

import static java.lang.Math.abs;

@Value
class PointRaster {

    int x;
    int y;

    boolean isFarFromTheCurrentMoreThen1Pixel(PointRaster point) {
        int dx = point.getX() - x;
        int dy = point.getY() - y;
        return abs(dx) > 1 || abs(dy) > 1;
    }
}
