package pl.designer.triangulation;

import pl.designer.Point2D;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.Math.round;

class PointRasterBuilder {

    private final int multiplier;

    public PointRasterBuilder(int factor) {
        checkArgument(factor > 0);
        this.multiplier = factor;
    }

    public PointRaster toRasterized(Point2D point2D) {
        int x = toMultipliedInt(point2D.getX());
        int y = toMultipliedInt(point2D.getY());
        return new PointRaster(x, y);
    }

    private int toMultipliedInt(double value) {
        return (int) round(value * multiplier);
    }
}
