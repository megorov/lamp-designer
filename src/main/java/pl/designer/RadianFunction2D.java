package pl.designer;

public interface RadianFunction2D {

  float getX(double radians);

  float getY(double radians);

  default Point2D getPoint(double radians) {
    return new Point2D(getX(radians), getY(radians));
  }
}
