package pl.designer;

import lombok.Value;

@Value
public class IndexedPoints2D {

    Point2D[] points;
    int[] indices;
}
