package pl.designer;

import lombok.Value;

@Value
public class AngleRange {

    int startAngle;
    int finishAngle;
}
