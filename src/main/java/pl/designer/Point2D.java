package pl.designer;

import lombok.Value;

@Value
public final class Point2D {

    float x;
    float y;

    public float getZ() {
        return 0;
    }

    public double getDistance(Point2D point) {
        float otherX = point.getX();
        float otherY = point.getY();

        float dX = otherX - x;
        float dY = otherY - y;

        return Math.hypot(dX, dY);
    }
}
