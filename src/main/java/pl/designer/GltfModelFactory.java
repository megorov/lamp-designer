package pl.designer;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.designer.contour.Contour2D;
import pl.designer.contour.Contour2DBuilder;
import pl.designer.contour.Contour2DConfiguration;
import pl.designer.triangulation.TriangulationBuilder;

import java.util.List;
import java.util.stream.Stream;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class GltfModelFactory {

    private final TriangulationBuilder triangulationBuilder;
    private final BinaryDataFactory binaryDataFactory;
    private final BoundedBoxFactory boundedBoxFactory;
    private final ByteArrayConverter byteArrayConverter;

    GltfModel create(AngleRange angleRange) {
        Contour2D contour = contour(angleRange);
        List<Triangle2D> triangles = triangulationBuilder.triangulate(contour);
        byte[] meshData = binaryDataFactory.toData(triangles);
        byte[] textureData = toTextureData(triangles);
        BoundBox boundBox = boundedBoxFactory.create(contour);
        return GltfModel.builder()
                .trianglesCount(triangles.size())
                .meshData(meshData)
                .textureData(textureData)
                .boundBox(boundBox)
                .build();
    }

    private byte[] toTextureData(List<Triangle2D> triangles) {
        List<Float> coordinates = triangles.stream()
                .flatMap(this::toFloatStream)
                .collect(toList());
//        List<Float> coordinates = Stream.of(new Point2D(0, 0), new Point2D(1, 0), new Point2D(1, 1), new Point2D(0, 1))
//                .flatMap(p -> Stream.of(p.getX(), p.getY()))
//                .collect(toList());
        return byteArrayConverter.toByteArray(coordinates);
    }

    private Stream<Float> toFloatStream(Triangle2D triangle) {
        return Stream.of(triangle.getP1(), triangle.getP2(), triangle.getP3())
                .flatMap(p -> Stream.of(p.getX(), p.getY()));
    }

    private Contour2D contour(AngleRange angleRange) {
        RadianFunction2D radianFunction2D = radianFunction2D();
        Contour2DConfiguration configuration = new Contour2DConfiguration(radianFunction2D, 0.5, angleRange);
        Contour2DBuilder factory = contour2DBuilder(configuration);
        return factory.build();
    }

    private Contour2DBuilder contour2DBuilder(Contour2DConfiguration configuration) {
        return Contour2DBuilder.builder()
                    .configuration(configuration)
                    .build();
    }

    private RadianFunction2D radianFunction2D() {
        return new RadianFunction2D() {
            @Override
            public float getX(double radians) {
                return (float) (1. * cos(radians));
            }

            @Override
            public float getY(double radians) {
                return (float) (1. * sin(radians));
            }
        };
    }
}
