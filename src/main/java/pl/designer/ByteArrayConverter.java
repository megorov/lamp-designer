package pl.designer;

import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.nio.ByteBuffer;
import java.util.List;

import static java.nio.ByteOrder.LITTLE_ENDIAN;

@Service
class ByteArrayConverter {

    public byte[] toByteArray(@NonNull List<Float> values) {
        ByteBuffer buffer = ByteBuffer.allocate(4 * values.size()).order(LITTLE_ENDIAN);
        values.forEach(buffer::putFloat);
        return buffer.array();
    }
}
