package pl.designer;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class GltfController {

    private final GltfModelFactory factory;

    @CrossOrigin
    @GetMapping("/gltf-model")
    public String getModel(@RequestParam("startAngle") int startAngle,
                           @RequestParam("finishAngle") int finishAngle) throws IOException {

        var angleRange = new AngleRange(startAngle, finishAngle);
        var gltfModel = factory.create(angleRange);
        return gltfModel.toJson();
    }
}
