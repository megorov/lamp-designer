package pl.designer;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class IndexedPoints2DBuilder {

    IndexedPoints2D build(List<Triangle2D> triangles) {
        var pointList = toPoints(triangles);
        var points = new LinkedHashSet<>(pointList).toArray(new Point2D[0]);
        var pointMap = getPointMap(points);
        int[] indices = getPointIndices(pointList, pointMap);
        return new IndexedPoints2D(points, indices);
    }

    private int[] getPointIndices(List<Point2D> pointList, Map<Point2D, Integer> pointMap) {
        return pointList.stream()
                .map(pointMap::get)
                .mapToInt(i -> i)
                .toArray();
    }

    private Map<Point2D, Integer> getPointMap(Point2D[] points) {
        Map<Point2D, Integer> pointMap = new HashMap<>();
        for (int i = 0; i < points.length; i++) {
            var point = points[i];
            pointMap.put(point, i);
        }
        return pointMap;
    }

    private List<Point2D> toPoints(List<Triangle2D> triangles) {
        return triangles.stream()
                .flatMap(this::getPointStream)
                .collect(Collectors.toList());
    }

    private Stream<Point2D> getPointStream(Triangle2D t) {
        return Stream.of(t.getP1(), t.getP2(), t.getP3());
    }
}
