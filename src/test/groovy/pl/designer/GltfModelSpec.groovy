package pl.designer

import groovy.json.JsonSlurper
import spock.lang.Specification

class GltfModelSpec extends Specification {

    GltfModel.GltfModelBuilder builder = new GltfModel.GltfModelBuilder()

    void 'should build proper gltf json'() {
        given:
            byte[] meshData = [0]
            byte[] textureData = [0]
            BoundBox box = new BoundBox(new Point2D(1, 1), new Point2D(-1, -1))
        when:
            GltfModel model = builder
                    .meshData(meshData)
                    .textureData(textureData)
                    .trianglesCount(meshData.length)
                    .boundBox(box)
                    .build()
            String json = model.toJson()
        then:
            Map<String, ?> map = new JsonSlurper().parseText(json) as Map<String, ?>
        and:
            with(map) {
                asset.version == '2.0'
                scenes[0].nodes == [0]
                nodes[0].mesh == 0
            }
        and:
            map.meshes.size == 1
            with(map.meshes[0].primitives[0]) {
                attributes.POSITION == 1
                attributes.TEXCOORD_0 == 0
                mode == 4
            }
        and:
            map.buffers.size == 2
            with(map.buffers[0]) {
                uri == 'data:application/gltf-buffer;base64,AA=='
                byteLength == 1
            }
            with(map.buffers[1]) {
                uri == 'data:application/gltf-buffer;base64,AA=='
                byteLength == 1
            }
        and:
            map.bufferViews.size == 2
            with(map.bufferViews[0]) {
                buffer == 0
                byteOffset == 0
                byteLength == 1
                target == 34962
            }
            with(map.bufferViews[1]) {
                buffer == 1
                byteOffset == 0
                byteLength == 1
                target == 34962
            }
        and:
            map.accessors.size == 2
            with(map.accessors[0]) {
                bufferView == 0
                byteOffset == 0
                componentType == 5126
                count == 3
                type == 'VEC2'
            }
            with(map.accessors[1]) {
                bufferView == 1
                byteOffset == 0
                componentType == 5126
                count == 3
                type == 'VEC3'
                max == [1, 1, 0]
                min == [-1, -1, 0]
            }
        and:
            map.samplers.size == 1
            with(map.samplers[0]) {
                magFilter == 9729
                minFilter == 9987
                wrapS == 10497
                wrapT ==10497
            }
        and:
            map.textures.size == 1
            with(map.textures[0]) {
                sampler == 0
                source == 0
            }
        and:
            map.images.size == 1
            map.images[0].uri == "../images/wood-600.jpg"
        and:
            map.materials.size == 1
            with(map.materials[0]){
                pbrMetallicRoughness.baseColorTexture.index == 0
                pbrMetallicRoughness.metallicFactor == 0.0
                pbrMetallicRoughness.roughnessFactor == 1.0
                doubleSided == true
            }
    }
}
