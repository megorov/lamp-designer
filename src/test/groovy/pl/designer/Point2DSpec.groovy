package pl.designer

import spock.lang.Specification
import spock.lang.Unroll

import static java.lang.Math.sqrt
import static java.math.RoundingMode.CEILING

@Unroll
class Point2DSpec extends Specification {

    void 'should calculate distance between O and point (#x, #y)'() {
        given:
            Point2D point = new Point2D(0, 0)
        expect:
            distance as double == point.getDistance(new Point2D(x, y))
        where:
            x    | y    | distance
            0    | 100  | 100
            100  | 0    | 100
            -100 | -100 | sqrt(20000)
    }

    private static BigDecimal round(double value) {
        return new BigDecimal(value).setScale(5, CEILING)
    }
}
