package pl.designer.contour

import pl.designer.AngleRange
import pl.designer.RadianFunction2D
import spock.lang.Specification

import static java.lang.Math.cos
import static java.lang.Math.sin
import static java.math.RoundingMode.CEILING

class Contour2DBuilderSpec extends Specification {

    void 'should build a contour with the correct length'() {
        given:
            RadianFunction2D function = new RadianFunction2D() {
                @Override
                float getX(double radians) {
                    return 100 * cos(radians)
                }

                @Override
                float getY(double radians) {
                    return 100 * sin(radians)
                }
            }
            Contour2DConfiguration configuration = new Contour2DConfiguration(
                    function,
                    0.3d,
                    new AngleRange(0, 360))
            Contour2DBuilder builder = new Contour2DBuilder(configuration)
        when:
            Contour2D contour2D = builder.build()
        then:
            round(contour2D.length) == round(2 * Math.PI * 100)
    }

    private static BigDecimal round(double length) {
        return new BigDecimal(length).setScale(3, CEILING)
    }
}
