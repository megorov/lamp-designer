package pl.designer.contour

import pl.designer.Point2D
import spock.lang.Specification

class Contour2DSpec extends Specification {

    void 'should calculate contour length correctly'() {
        given:
            Contour2D contour2D = new Contour2D([point(0, 0),
                                                 point(0, 100),
                                                 point(-100, 100),
                                                 point(-100, 0)
            ])
        expect:
            contour2D.length == 400
    }

    private static Point2D point(Integer x, Integer y) {
        return new Point2D(x, y)
    }
}
