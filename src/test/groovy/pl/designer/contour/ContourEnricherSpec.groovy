package pl.designer.contour

import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

@Unroll
class ContourEnricherSpec extends Specification {

    DistanceChecker distanceChecker = new DistanceChecker<Integer>() {
        @Override
        boolean ifPointsTooFar(Integer current, Integer next) {
            return next - current > 1
        }
    }

    NewPointCreator newPointCreator = new NewPointCreator<Integer>() {

        @Override
        Integer create(Integer p1, Integer p2) {
            return ((p2 - p1) / 2 + p1)
        }
    }

    @Subject
    ContourEnricher<Integer> enricher = new ContourEnricher<Integer>(distanceChecker, newPointCreator)


    void 'should build contour correctly when seed: #seed'() {
        when:
            List<Integer> result = enricher.getContour(seed as List<Integer>)
        then:
            result == [0, 1, 2, 3, 4, 5]
        where:
            seed << [[0, 5], [0, 1, 2, 5], [0, 4, 5], [0, 1, 2, 3, 4, 5]]
    }

    void 'should build contour fast enough'() {
        when:
            long startTime = System.currentTimeMillis()
            (0..10_000).each {
                enricher.getContour([0, 4, 5])
            }
        then:
            System.currentTimeMillis() - startTime < 300
    }

    void 'should throw exception if seed collection contains less then 2 elements'() {
        when:
            enricher.getContour(seed as List<Integer>)
        then:
            IllegalArgumentException ex = thrown(IllegalArgumentException)
            ex.message == 'Seed points collection should have at lease 2 elements'
        where:
            seed << [[], [1]]
    }
}
