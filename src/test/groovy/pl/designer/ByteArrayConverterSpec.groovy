package pl.designer

import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

@Unroll
class ByteArrayConverterSpec extends Specification {
    @Subject
    ByteArrayConverter converter = new ByteArrayConverter()

    void 'should properly convert #values to #result'() {
        expect:
            converter.toByteArray(values) == result as byte[]
        where:
            values       || result
            [0f]         || [0, 0, 0, 0]
            [1f]         || [0, 0, -128, 63]
            [1f, 1f]     || [0, 0, -128, 63, 0, 0, -128, 63]
            [0f, 1f, 0f] || [0, 0, 0, 0, 0, 0, -128, 63, 0, 0, 0, 0]
    }
}
