package pl.designer.triangulation

import pl.designer.Point2D
import pl.designer.contour.Contour2D
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

@Unroll
class ContourRasterBuilderSpec extends Specification {

    PointRasterBuilder pointRasterBuilder = new PointRasterBuilder(1000)

    @Subject
    ContourRasterBuilder builder = new ContourRasterBuilder(pointRasterBuilder)

    void 'should build proper raster contour from #points'() {
        when:
            Contour2D contour2D = createContour2D(points as float [][])
        and:
            ContourRaster result = builder.toRasterized(contour2D)
        then:
            result.pointStream.collect() == createPointRaster(pointsRaster as int [][])
        where:
            points                                           || pointsRaster
            [[1.001, 1.001], [1.001, 1.002]]                 || [[1001, 1001], [1001, 1002]]
            [[1.001, 1.001], [1.001, 1.002], [1.002, 1.002]] || [[1001, 1001], [1001, 1002], [1002, 1002]]
    }

    void 'should build raster contour with distinct points from points #points'() {
        when:
            Contour2D contour2D = createContour2D(points as float [][])
        and:
            ContourRaster result = builder.toRasterized(contour2D)
        then:
            result.pointStream.collect() == createPointRaster(pointsRaster as int [][])
        where:
            points                                                            || pointsRaster
            [[1.001, 1.001], [1.001, 1.001], [1.001, 1.002]]                  || [[1001, 1001], [1001, 1002]]
            [[1.001, 1.001], [1.001, 1.002], [1.001, 1.001]]                  || [[1001, 1001], [1001, 1002]]
            [[1.001, 1.001], [1.001, 1.002], [1.001, 1.001], [1.001, 1.002],] || [[1001, 1001], [1001, 1002]]
    }

    void 'should build raster contour with enriched points from points #points'() {
        when:
            Contour2D contour2D = createContour2D(points as float [][])
        and:
            ContourRaster result = builder.toRasterized(contour2D)
        then:
            result.pointStream.collect() == createPointRaster(pointsRaster as int [][])
        where:
            points                                           || pointsRaster
            [[1.001, 1.001], [1.003, 1.001]]                 || [[1001, 1001], [1002, 1001], [1003, 1001]]
            [[1.001, 1.001], [1.003, 1.001], [1.003, 1.003]] || [[1001, 1001], [1002, 1001], [1003, 1001], [1003, 1002], [1003, 1003]]
    }

    private static Contour2D createContour2D(float  [][] coordinates) {
        return new Contour2D(coordinates.collect { new Point2D(it[0], it[1]) })
    }

    private static List<PointRaster> createPointRaster(int [][] coordinates) {
        return coordinates.collect { new PointRaster(it[0], it[1]) }
    }
}
