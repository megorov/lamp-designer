package pl.designer.triangulation

import pl.designer.Point2D
import pl.designer.Triangle2D
import pl.designer.contour.Contour2D
import spock.lang.Specification
import spock.lang.Subject

import static java.lang.Math.*

class TriangulationBuilderSpec extends Specification {

    @Subject
    TriangulationBuilder builder = new TriangulationBuilder()

    void 'should build triangulation of convex polygon'() {
        given:
            Contour2D contour2D = new Contour2D(List.of(
                    new Point2D(0, 0),
                    new Point2D(100, 0),
                    new Point2D(100, 100),
                    new Point2D(0, 100)
            ))
        when:
            List<Triangle2D> triangulated = builder.triangulate(contour2D)
        then:
            triangulated.size() == 2
    }

    void 'should build triangulation of concave polygon'() {
        given:
            Contour2D contour2D = new Contour2D(List.of(
                    new Point2D(0, 0),
                    new Point2D(100, 0),
                    new Point2D(100, 100),
                    new Point2D(0, 100),
                    new Point2D(50, 50)
            ))
        when:
            List<Triangle2D> triangulated = builder.triangulate(contour2D)
        then:
            triangulated.size() == 3
    }

    void 'should build triangulation of circle polygon'() {
        given:
            double radius = 1000
        and:
            int circleArea = (int) (PI * radius * radius)
        and:
            List<Point2D> points = (1..500).collect {
                double step = 2 * PI / 500
                float x = radius * cos(it * step) as float
                float y = radius * sin(it * step) as float
                return new Point2D(x, y)
            }
        and:
            Contour2D contour2D = new Contour2D(points)
        when:
            List<Triangle2D> triangulated = builder.triangulate(contour2D)
        then:
            int area = triangulated.collect {
                Triangle2D t = it
                double a = t.p1.getDistance(t.p2)
                double b = t.p2.getDistance(t.p3)
                double c = t.p3.getDistance(t.p1)
                double p = (a + b + c) / 2
                return sqrt(p * (p - a) * (p - b) * (p - c))
            }.sum() as Integer
            area <= circleArea && area > circleArea - circleArea/10000
    }
}
