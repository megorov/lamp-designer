package pl.designer.triangulation

import pl.designer.Point2D
import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class PointRasterBuilderSpec extends Specification {

    void 'should build (#resultX, #resultY) properly from (#dx, #dy)'() {
        given:
            PointRasterBuilder builder = new PointRasterBuilder(factor)
        when:
            PointRaster pointRasterized = builder.toRasterized(new Point2D(dx, dy))
        then:
            with(pointRasterized) {
                x == resultX
                y == resultY
            }
        where:
            dx    | dy    | factor || resultX | resultY
            1.001 | 1.001 | 1000   || 1001    | 1001
            0.001 | 0.001 | 1000   || 1       | 1
            1.001 | 1.001 | 100    || 100     | 100
            1.01  | 1.01  | 100    || 101     | 101
    }

    void 'should throw exception when multiplier #multiplier is not positive'() {
        when:
            new PointRasterBuilder(factor)
        then:
            thrown(IllegalArgumentException)
        where:
            factor << [0, -10]
    }
}
