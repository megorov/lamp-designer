package pl.designer

import spock.lang.Specification
import spock.lang.Subject

class IndexedPoints2DBuilderSpec extends Specification {

    @Subject
    IndexedPoints2DBuilder builder = new IndexedPoints2DBuilder()

    void 'should create indexed points'(){
        given:
            List<Triangle2D> triangles = [
                    new Triangle2D(new Point2D(0, 0), new Point2D(0, 1), new Point2D(1, 1)),
                    new Triangle2D(new Point2D(1, 1), new Point2D(1, 0), new Point2D(0, 0))
            ]
        when:
            IndexedPoints2D indexedPoints = builder.build(triangles)
        then:
            with(indexedPoints){
                points == [new Point2D(0, 0), new Point2D(0, 1), new Point2D(1, 1), new Point2D(1, 0)] as Point2D[]
                indices == [0, 1, 2, 2, 3, 0] as int[]
            }
    }
}
