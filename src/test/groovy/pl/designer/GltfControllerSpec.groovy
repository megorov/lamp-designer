package pl.designer

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.mock.DetachedMockFactory

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(GltfController)
class GltfControllerSpec extends Specification {

    @Autowired
    private GltfModelFactory factory

    @Autowired
    private MockMvc mvc

    void 'should propagate gltf json from builder'() {
        given:
            String responseContent = createContent()
        and:
            GltfModel gltfModel = Stub() {
                toJson() >> responseContent
            }
            factory.create(_ as AngleRange) >> gltfModel
        expect:
            mvc.perform(get("/api/gltf-model")
                    .param("startAngle", "0")
                    .param("finishAngle", "360"))
                    .andExpect(status().isOk())
                    .andExpect(content().json(responseContent))
    }

    String createContent() {
        return '''{
                  "scenes" : [
                    {
                      "nodes" : [ 0 ]
                    }
                  ],
                
                  "nodes" : [
                    {
                      "mesh" : 0
                    }
                  ],
                
                  "meshes" : [
                    {
                      "primitives" : [ {
                        "attributes" : {
                          "POSITION" : 0
                        },
                        "mode": 5
                      } ]
                    }
                  ],
                
                  "buffers" : [
                    {
                      "uri" : "data2.bin",
                      "byteLength" : 48
                    }
                  ],
                  "bufferViews" : [
                    {
                      "buffer" : 0,
                      "byteOffset" : 0,
                      "byteLength" : 48,
                      "target" : 34962
                    }
                  ],
                  "accessors" : [
                    {
                      "bufferView" : 0,
                      "byteOffset" : 0,
                      "componentType" : 5126,
                      "count" : 4,
                      "type" : "VEC3",
                      "max" : [ 1.0, 1.0, 0.0 ],
                      "min" : [ 0.0, 0.0, 0.0 ]
                    }
                  ],
                
                  "asset" : {
                    "version" : "2.0"
                  }
                }'''
    }

    @TestConfiguration
    static class StubConfig {

        DetachedMockFactory detachedMockFactory = new DetachedMockFactory()

        @Bean
        GltfModelFactory gltfModel() {
            return detachedMockFactory.Stub(GltfModelFactory)
        }
    }
}
