package pl.designer

import pl.designer.contour.Contour2D
import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class BoundedBoxFactorySpec extends Specification {

    void 'should create correct bounded box for #points'() {
        given:
            List<Point2D> pointList = points.collect{new Point2D(it[0] as float, it[1] as float)}
            Contour2D contour = new Contour2D(pointList)
        when:
            BoundBox result = new BoundedBoxFactory().create(contour)
        then:
            with(result) {
                max == maxPoint
                min == minPoint
            }
        where:
            points                         || maxPoint            | minPoint
            [[2.2, -1], [1, 0], [-1.1, 0]] || new Point2D(2.2, 0) | new Point2D(-1.1, -1)
            [[0, 0], [1, 1], [-1, -1]]     || new Point2D(1, 1)   | new Point2D(-1, -1)
            [[0, -1], [1, 0], [-1, 0]]     || new Point2D(1, 0)   | new Point2D(-1, -1)
    }
}
