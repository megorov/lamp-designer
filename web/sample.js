
			import * as THREE from '/build/three.module.js';

			import { OrbitControls } from '/jsm/controls/OrbitControls.js';
			import { GLTFLoader } from '/jsm/loaders/GLTFLoader.js';

			var container, stats, controls;
			var camera, scene, renderer;

			init();
			animate();

			function init() {

				container = document.createElement( 'div' );
				document.body.appendChild( container );

				camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 0.25, 20 );
				camera.position.set( - 1.8, 0.9, 2.7 );

				scene = new THREE.Scene();
				scene.background = new THREE.Color( 0x808080);

				var loader = new GLTFLoader();//.setPath( 'models/gltf/Sample/' );
//                						loader.load( 'sample.gltf', function ( gltf ) {
                						loader.load( 'http://localhost:8080/api/gltf-model?startAngle=0&finishAngle=90', function ( gltf ) {


                							gltf.scene.traverse( function ( child ) {

                								if ( child.isMesh ) {

                									//child.material.envMap = envMap;

                								}

                							} );

                							scene.add( gltf.scene );

                						} );


				renderer = new THREE.WebGLRenderer( { antialias: true } );
				renderer.setPixelRatio( window.devicePixelRatio );
				renderer.setSize( window.innerWidth, window.innerHeight );
				renderer.gammaOutput = true;
				container.appendChild( renderer.domElement );

				controls = new OrbitControls( camera, renderer.domElement );
				controls.target.set( 0, - 0.2, - 0.2 );
				controls.update();

				window.addEventListener( 'resize', onWindowResize, false );

			}

			function onWindowResize() {

				camera.aspect = window.innerWidth / window.innerHeight;
				camera.updateProjectionMatrix();

				renderer.setSize( window.innerWidth, window.innerHeight );

			}

			function animate() {

				requestAnimationFrame( animate );

				renderer.render( scene, camera );

			}
